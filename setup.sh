#!/bin/bash
echo "185.199.108.133 raw.githubusercontent.com" >> /etc/hosts
cd /var/www/html
composer install
yarn install
yarn dev
php  bin/console doctrine:migrations:migrate -n
php  bin/console app:load-products
php-fpm