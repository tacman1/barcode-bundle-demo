ARG PHP_VERSION=8.1
ARG NGINX_VERSION=1.18.0

FROM php:${PHP_VERSION}-fpm-alpine

RUN apk add --update libzip-dev curl-dev git &&\
    docker-php-ext-install curl && \
    apk del gcc g++ &&\
    rm -rf /var/cache/apk/*
RUN apk add --no-cache autoconf g++ make
RUN apk add --no-cache postgresql-dev && docker-php-ext-install pdo_pgsql

RUN apk update && apk add bash

RUN apk add --no-cache freetype-dev libjpeg-turbo-dev libpng-dev
RUN docker-php-ext-install gd
RUN apk add --no-cache imagemagick-dev
RUN pecl install imagick && docker-php-ext-enable imagick

RUN docker-php-ext-install sockets curl

RUN apk add --no-cache zip libzip-dev
RUN docker-php-ext-configure zip
RUN docker-php-ext-install zip

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

ADD . /var/www/html
COPY setup.sh /usr/local/bin/setup
RUN chmod +x /usr/local/bin/setup

WORKDIR /var/www/html

RUN wget https://get.symfony.com/cli/installer -O - | bash
RUN export PATH="$HOME/.symfony5/bin:$PATH"

RUN mv /usr/local/etc/php/php.ini-development /usr/local/etc/php/php.ini
RUN sed -ri -e 's!;date.timezone =!date.timezone = "America\/Los_Angeles"!g' /usr/local/etc/php/php.ini

RUN apk add --no-cache nodejs npm
RUN npm install -g yarn

ENTRYPOINT ["/usr/local/bin/setup"]